<?php

namespace Acme\LibraryBundle\Entity;

use Acme\Library\Port\BookRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Acme\Library\Model\Book as BaseBook;

class BookRepository extends EntityRepository implements BookRepositoryInterface
{
    public function save(BaseBook $book)
    {
        $this->_em->persist($book);
        $this->_em->flush();
    }

    public function searchBooks(Array $searchParameters)
    {
        $searchQuery = $this->_em->createQueryBuilder()->select('b')->from($this->_entityName,'b');

        if(isset($searchParameters['rating'])){
            $searchQuery = $searchQuery->andWhere('b.rating > :rating')
                        ->setParameter('rating',$searchParameters['rating']);
        }

        if(isset($searchParameters['isbn'])){
            $searchQuery = $searchQuery->andWhere('b.isbn = :isbn')
                ->setParameter('isbn',$searchParameters['isbn']);
        }

        if(isset($searchParameters['title'])){
            $searchQuery = $searchQuery->andWhere('b.title LIKE :title')
                ->setParameter('title','%'.$searchParameters['title'].'%');
        }

        if(isset($searchParameters['author'])){
            $searchQuery = $searchQuery->leftJoin('b.Author' ,'a')->andWhere('a.name LIKE :author OR a.surname LIKE :author')
                ->setParameter('author','%'.$searchParameters['author'].'%');
        }

        if(isset($searchParameters['created_from'])){
            $searchQuery = $searchQuery->andWhere('b.created_date > :created_from')
                ->setParameter('created_from',(new \DateTime($searchParameters['created_from']))->format('Y-m-d'));
        }

        if(isset($searchParameters['created_to'])){
            $searchQuery = $searchQuery->andWhere('b.created_date < :created_to')
                ->setParameter('created_to',(new \DateTime($searchParameters['created_to']))->format('Y-m-d'));
        }

        $query = $searchQuery->getQuery();
        return $query->getResult();
    }
}
