<?php
namespace Acme\LibraryBundle\Controller;

use Acme\Library\Port\BookProvider;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;

class BookController
{

    public function __construct(
        Request $request,
        ViewHandler $viewHandler,
        BookProvider $books
    ) {
        $this->request = $request;
        $this->viewHandler = $viewHandler;
        $this->books = $books;
    }

    /**
     * Collection get action
     * @var Request $request
     * @return View
     */
    public function getBooksAction()
    {
        $view = View::create();
        $view->setData($this->books->findAll());
        $view->setStatusCode(200);
        return $this->viewHandler->handle($view);
    }

    public function getBooksSearchAction()
    {
        $view = View::create();
        $view->setData($this->books->searchBooks($this->request->query->all()));
        $view->setStatusCode(200);
        return $this->viewHandler->handle($view);
    }

}