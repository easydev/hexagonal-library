<?php

namespace Acme\LibraryBundle\DataFixtures\ORM;

use Acme\LibraryBundle\Entity\Author;
use Acme\LibraryBundle\Entity\Book;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class LoadBook implements ContainerAwareInterface,FixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $author = new Author('Mateusz','Jendrzyn');
        $manager->persist($author);

        $first_book = new Book('lorem ipsum','978-3-16-148410-0','5.0',new \DateTime(),$author);
        $manager->persist($first_book);

        $second_book = new Book('lorem ipsum lorem ipsum','918-3-16-148410-0','4.0',new \DateTime(),$author);
        $manager->persist($second_book);

        $manager->flush();
    }
}
