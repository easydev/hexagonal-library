<?php
namespace Acme\Library\Port;

use Acme\Library\Model\Author;

interface AuthorRepositoryInterface {
    public function save(Author $author);
    public function getBooksByAuthor();
}