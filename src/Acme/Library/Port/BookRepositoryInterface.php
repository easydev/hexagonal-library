<?php
namespace Acme\Library\Port;

use Acme\Library\Model\Book;

interface BookRepositoryInterface {
    public function save(Book $book);
    public function findAll();
    public function searchBooks(Array $searchParameters);
}