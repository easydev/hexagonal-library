<?php
namespace Acme\Library\Port;

use Acme\Library\Port\BookRepositoryInterface;

class BookProvider
{

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function findAll()
    {
        return $this->bookRepository->findAll();
    }

    public function searchBooks(Array $searchParameters)
    {
        return $this->bookRepository->searchBooks($searchParameters);
    }
}

