<?php

namespace Acme\Library\Model;

class Book
{
    protected $title;
    protected $Author;
    protected $isbn;
    protected $rating;
    protected $created_date;

    public function __construct($title, $isbn, $rating, $created_date, Author $author)
    {
        $this->title = $title;
        $this->Author = $author;
        $this->isbn = $isbn;
        $this->rating = $rating;
        $this->created_date = $created_date;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getIsbn()
    {
        return $this->isbn;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function getCreatedDate()
    {
        return $this->created_date;
    }

    public function getAutor()
    {
        return $this->Author;
    }
}
