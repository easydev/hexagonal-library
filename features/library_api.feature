Feature: Returning books using api with query options

  In order to be able to search books
  As a library system developer
  I need to be able to call api

Scenario: Getting all books
  Given I send a GET request to "api/books"
  Then the response should be in JSON
  And the JSON node "root[0].title" should contain "lorem ipsum"
  And the JSON node "root[1].title" should contain "lorem ipsum"

Scenario: Getting books with specyfic rating
  Given I send a GET request to "api/books/search?rating=4"
  Then the response should be in JSON
  And the JSON node "root[0].rating" should contain "5"

Scenario: Getting books with specyfic title
  Given I send a GET request to "api/books/search?title=lorem"
  Then the response should be in JSON
  And the JSON node "root[0].title" should contain "lorem"
  And the JSON node "root" should have 2 elements

Scenario: Getting books created between specyfic date
  Given I send a GET request to "api/books/search?created_from=01.01.1999&created_to=01.01.2015"
  Then the response should be in JSON
  And the JSON node "root[0].title" should contain "lorem"
  And the JSON node "root" should have 2 elements

Scenario: Getting books created by author
  Given I send a GET request to "api/books/search?author=Mateusz"
  Then the response should be in JSON
  And the JSON node "root[0]._author.name" should contain "Mateusz"
  And the JSON node "root" should have 2 elements

Scenario: Getting books with specyfic isbn number
  Given I send a GET request to "api/books/search?isbn=978-3-16-148410-0"
  Then the response should be in JSON
  And the JSON node "root[0].isbn" should contain "978-3-16-148410-0"
  And the JSON node "root" should have 1 elements