<?php
namespace spec\Acme\Library\Port;

use Acme\Library\Model\Book;
use Acme\Library\Port\BookRepositoryInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookProviderSpec extends ObjectBehavior
{
    function let(BookRepositoryInterface $repository)
    {
        $this->beConstructedWith($repository);
    }

    function it_retrieves_all_books_using_repository(
        Book $first_book,
        Book $second_book,
        BookRepositoryInterface $repository
    )
    {
        $repository->findAll()->willReturn([$first_book, $second_book]);

        $this->findAll()->shouldReturn([$first_book, $second_book]);
    }

    function it_search_books_using_repository(
        Book $first_book,
        Book $second_book,
        BookRepositoryInterface $repository
    )
    {
        $searchParameters = ['rating' => 4];
        $repository->searchBooks($searchParameters)->willReturn([$second_book]);

        $this->searchBooks($searchParameters)->shouldReturn([$second_book]);
    }
}
