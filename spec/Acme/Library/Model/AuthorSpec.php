<?php

namespace spec\Acme\Library\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AuthorSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('a name','a surname');
    }

    function it_has_a_name()
    {
        $this->getName()->shouldReturn('a name');
    }

    function it_has_a_surname()
    {
        $this->getSurname()->shouldReturn('a surname');
    }

}
