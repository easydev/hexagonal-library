<?php

namespace spec\Acme\Library\Model;

use Acme\Library\Model\Author;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookSpec extends ObjectBehavior
{

    function let(Author $author)
    {
        $this->beConstructedWith('a title', 'isbn', 'rating', new \DateTime('06.09.2014'), $author);
    }

    function it_has_a_title()
    {
        $this->getTitle()->shouldReturn('a title');
    }

    function it_has_isbn_number(){
        $this->getIsbn()->shouldReturn('isbn');
    }

    function it_has_rating(){
        $this->getRating()->shouldReturn('rating');
    }

    function it_has_created_date()
    {
        $created_date = $this->getCreatedDate();
        $created_date->shouldBeAnInstanceOf('DateTime');
        $created_date->format('d.m.Y')->shouldReturn('06.09.2014');
    }

    function it_has_a_link_to_author(Author $author)
    {
        $this->getAutor()->shouldReturn($author);
    }
}
